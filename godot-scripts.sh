#!/usr/bin/env bash

if [[ $0 != *bash* && $0 != *zsh* ]]; then
    echo
    echo "Usage: . $0"
    echo
    cat $0 | grep "^function g-.*" | sed "s/^function \(s-[a-zA-Z0-9-]*\).*{\(.*\)/+ \1:\2/g" | sed "s/: #/:/g" | sort |
    while read line
    do
        DESC=${line#*:}
        COMMAND=${line%%:*}
        printf " %-40s:%s\n" "$COMMAND" "$DESC"
    done
    echo ""
else
    echo Loaded s-\* functions.
fi

# Export all aseprite files in the RawAssets folder to png files in the Assets folder
# Todo: preserve directory structure between RawAssets and Assets folders?
function g-export-sprites {
    for sprite in $(ls RawAssets/*aseprite) ; do
        filename=$(basename $sprite .aseprite)
        aseprite --batch --sheet Assets/$filename.png RawAssets/$filename.aseprite
    done
}

# Print all the lines that configure a logger to allow debug logs
function g-find-debug-logs {
    grep --include \*.cs -r -E 'LogFactory\.For\(.*true.*\)' .
}

function g-build-engine {
    scons p=x11 target=release_debug tools=yes module_mono_enabled=yes -j10
}

function g-new {
	echo "Pull in changes to SpaceRPG.csproj/sln/etc before creating another project from this template"
	return 0

    template_project_name=GodotTemplateProject
    template_project_guid=B1D5A110-40AA-4EB9-8A1B-62FB047F5558

    project_name=$1
    project_guid=$(uuidgen --random | tr '[:lower:]' '[:upper:]')
    if [ -z $project_name ]; then
        echo "Please provide a project name."
        return 0
    fi

    git clone git@git.sr.ht:~spencewenski/$template_project_name $project_name

    pushd $project_name > /dev/null
    rm README.md
    mv README-template.md README.md
    grep -rl $template_project_name . | xargs sed -i -e "s/$template_project_name/$project_name/g"
    grep -rl $template_project_guid . | xargs sed -i -e "s/$template_project_guid/$project_guid/g"
    rename $template_project_name $project_name **/**
    git remote set-url origin git@git.sr.ht:~spencewenski/$project_name
    git add .
    git commit -m "Init from template"
    popd > /dev/null
}
