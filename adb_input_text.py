#!/usr/bin/env python3

"""
Take the provided text and send it to the attached device using adb.

Will still need to manually escape the following in order to pass them
as an argument to this script on the command line: !, ", and `, e.g.:

"\! \` \""
"""

import sys
import argparse

class String_constants:
  NEWLINE = '\n'


def safe_print(string, min_verbosity=0, end=String_constants.NEWLINE):
  # if min_verbosity > args.verbosity:
  #   return
  print(string, end=end)


def execute_command(command, min_verbosity = 0):
  import subprocess
  process = subprocess.Popen(command.split(), stdout=subprocess.PIPE,
    stderr=subprocess.STDOUT, universal_newlines=True)
  for line in process.stdout:
    safe_print(line, min_verbosity=min_verbosity, end="")


def get_replacement(char):
  if char == "'":
    return """'"\'"'"""
  if char == " ":
    return """%s"""
  if char == "(":
    return """'"("'"""
  if char == ")":
    return """'")"'"""
  return char

def sanitize_text(text):
  result = ""
  for char in text:
    result = result + get_replacement(char)
  return result


def parse_command_line_args(argv):
  parser = argparse.ArgumentParser(__doc__)
  parser.add_argument('text', metavar='TEXT', help='Text to send to the device.')
  return parser.parse_args(argv)


def main(argv):
  global args
  args = parse_command_line_args(argv)
  execute_command("adb shell input text '" + sanitize_text(args.text) + "'")
  # print(sanitize_text(args.text))


if __name__ == "__main__":
  main(sys.argv[1:])
