#!/usr/bin/env bash

SETUP_DIR_NAME=""
if [[ $(uname -s) == "Darwin" ]]; then
  SETUP_DIR_NAME="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
else
  SETUP_DIR_NAME="$(dirname $(readlink -f $0))"
fi

# https://stackoverflow.com/questions/592620/check-if-a-program-exists-from-a-bash-script
command_exists() {
    type "$1" > /dev/null 2>&1
}

directory_exists() {
  directory="$1"
    # Expand the '~' into the absolute path
    # https://stackoverflow.com/questions/3963716/how-to-manually-expand-a-special-variable-ex-tilde-in-bash
    directory="${directory/#\~/$HOME}"
    if [ -d "$directory" ]; then
        return 0
    else
        return 1
    fi
}

file_exists() {
    file_name="$1"
    # Expand the '~' into the absolute path
    # https://stackoverflow.com/questions/3963716/how-to-manually-expand-a-special-variable-ex-tilde-in-bash
    file_name="${file_name/#\~/$HOME}"
    if [ -e "$file_name" ]; then
        return 0
    else
        return 1
    fi
}

# Enable 'ln' in Cygwin and link windows 'D' drive folders to Cygwin home
if [[ $(uname -s) == *"CYGWIN"* ]]; then
  echo
  echo 'Linking windows "D" drive folders to Cygwin home'
  export CYGWIN="winsymlinks:native"
  ln -fsv /cygdrive/d/$USER/* $HOME
fi

mkdir -p $HOME/bin

ln -fsv "$SETUP_DIR_NAME/adb_input_text.py" "$HOME/bin/adbt"
ln -fsv "$SETUP_DIR_NAME/add_custom_keyboard_shortcut.py" "$HOME/bin/add_custom_keyboard_shortcut"
ln -fsv "$SETUP_DIR_NAME/find_file" "$HOME/bin/ff"
ln -fsv "$SETUP_DIR_NAME/resume" "$HOME/bin/resume"
ln -fsv "$SETUP_DIR_NAME/auto-resume" "$HOME/bin/auto-resume"
ln -fsv "$SETUP_DIR_NAME/email_alias" "$HOME/bin/email-alias"
ln -fsv "$SETUP_DIR_NAME/aur-install" "$HOME/bin/aur-install"
ln -fsv "$SETUP_DIR_NAME/aur-update" "$HOME/bin/aur-update"

# These are mostly just for Arch/i3
ln -fsv "$SETUP_DIR_NAME/suspend" "$HOME/bin/suspend"
ln -fsv "$SETUP_DIR_NAME/firefox_private" "$HOME/bin/firefox_private"
ln -fsv "$SETUP_DIR_NAME/screenshot" "$HOME/bin/screenshot"
ln -fsv "$SETUP_DIR_NAME/screenshot-select-area" "$HOME/bin/screenshot-select-area"
ln -fsv "$SETUP_DIR_NAME/keep_awake" "$HOME/bin/keep_awake"
ln -fsv "$SETUP_DIR_NAME/keep_awake_on" "$HOME/bin/keep_awake_on"
ln -fsv "$SETUP_DIR_NAME/keep_awake_off" "$HOME/bin/keep_awake_off"
